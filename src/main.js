import '@babel/polyfill'
import Vue from 'vue'
import App from './App'
import router from './router'
import VueMaterial from 'vue-material'
import Vuex from 'vuex'
import store from './store'
import VeeValidate from 'vee-validate'
import VLazyImagePlugin from 'v-lazy-image'
import './filters'
import './assets/theme.scss'
import 'vue-material/dist/vue-material.min.css'

Vue.use(VueMaterial)
Vue.use(Vuex)
Vue.use(VeeValidate)
Vue.component('v-lazy-image', VLazyImagePlugin)

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
