const defaultState = {
  loansData: [],
  loanRating: 0,
  loanRatingCode: ''
}

const actions = {

}

const mutations = {
  storeLoansRawData (defaultState, loansData) {
    defaultState.loansData = loansData
  },

  addMoreLoans (defaultState, addedLoansData) {
    defaultState.loansData.concat(addedLoansData)
  },

  storeLoanRating (defaultState, loanRatingNumber) {
    defaultState.loanRating = loanRatingNumber
  },

  storeLoanRatingCode (defaultState, loanRatingCode) {
    defaultState.loanRatingCode = loanRatingCode
  }
}

const getters = {

}

export default {
  state: defaultState,
  getters,
  actions,
  mutations
}
