import axios from 'axios'
import LoanList from './loanList.html'
import EventBus from '../../event-bus'
import LoadingSpinner from '../loadingSpinner/loadingSpinner'

const PROXY_ADDRESS = 'http://127.0.0.1:3000/pipe/'
const ZONKY_API_RATING = 'https://api.zonky.cz/loans/marketplace?rating__eq='
const API_ADDRESS = 'https://api.zonky.cz/'

export default LoanList({
  name: 'loanList',

  components: {
    LoadingSpinner
  },

  data: () => ({
    loansData: [],
    responseErrors: [],
    timesLoaded: 0,
    isLoading: true,
    bottom: false
  }),

  // TODO: DRY
  computed: {
    progressBarAmount () {
      return this.loansData.map((item) => {
        return item.remainingInvestment / item.amount * 100
      })
    },

    getLoanRating () {
      return this.loansData.map((item) => {
        let loanRatingData = item.rating

        return {
          'A': 'A',
          'AA': 'A+',
          'AAA': 'A++',
          'AAAA': 'A*',
          'AAAAA': 'A**',
          'B': 'B',
          'C': 'C',
          'D': 'D'
        }[loanRatingData]
      })
    },

    getLoanRatingCode () {
      return this.$store.state.loans.loanRatingCode
    }
  },

  created () {
    this.handleLoansData()
    window.addEventListener('scroll', this.handleScroll)
  },

  methods: {
    handleLoansData () {
      let ratingCode = this.getLoanRatingCode

      // CORS override via GoBetween CORS proxy
      axios.get(PROXY_ADDRESS + ZONKY_API_RATING + ratingCode, {
        headers: {
          'X-Page': this.timesLoaded,
          'X-Size': 20
        }
      })
        .then(request => {
          // If is page loaded for a first time
          if (this.timesLoaded < 1) {
            this.loansData = request.data
            this.$store.commit('storeLoansRawData', this.loansData)
            this.firstRun = false
          } else {
            this.$store.commit('addMoreLoans', request.data)
            this.loansData.push(...request.data)
          }
          this.timesLoaded += 1
          this.isLoading = false
        })
        .catch(e => {
          this.responseErrors.push(e)
        })
    },

    getImageUrl (index) {
      return API_ADDRESS + this.loansData[index].photos[0].url
    },

    handleScroll () {
      window.onscroll = () => {
        if ((window.innerHeight + window.pageYOffset) >= document.body.offsetHeight) {
          this.isLoading = true
          this.handleLoansData()
        }
      }
    }
  },

  mounted () {
    EventBus.$on('trigger-calculator', countLoanAmount => {
      this.timesLoaded = 0
      this.handleLoansData()
    })
  },

  destroyed () {
    window.removeEventListener('scroll', this.handleScroll)
  }
})
