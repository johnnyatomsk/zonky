import EventBus from '../../event-bus'
import LoanRatingFilter from './loanRatingFilter.html'

export default LoanRatingFilter({
  name: 'LoanRatingFilter',

  data: () => ({
    rating: [
      'A',
      'A+',
      'A++',
      'A*',
      'A**',
      'B',
      'C',
      'D'
    ],
    ratingCode: [
      'A',
      'AA',
      'AAA',
      'AAAA',
      'AAAAA',
      'B',
      'C',
      'D'
    ],
    selected: 0
  }),

  methods: {
    getLoanRating () {
      this.$store.commit('storeLoanRating', this.selected)
    },

    getLoanRatingCode () {
      this.$store.commit('storeLoanRatingCode', this.ratingCode[this.selected])
      EventBus.$emit('trigger-calculator', this.countLoanAmount)
    }
  }
})
