import EventBus from '../../event-bus'
import ZonkieHeader from './zonkieHeader.html'
import ZonkieSupport from '../zonkieSupport/zonkieSupport'

export default ZonkieHeader({
  name: 'ZonkieHeader',

  components: {
    ZonkieSupport
  },

  data: () => ({
    showNavigation: false,
    showSidepanel: false,
    showDialog: false,
    showConfirmation: false
  }),

  methods: {
    toggleModal () {
      this.showDialog = !this.showDialog
    },

    toggleNavigation () {
      this.showNavigation = !this.showNavigation
    },

    openSupportModal () {
      this.toggleModal()
      this.toggleNavigation()
    }
  },

  mounted () {
    EventBus.$on('trigger-modal', modalState => {
      this.showDialog = modalState
      this.showConfirmation = true
    })
  }
})
