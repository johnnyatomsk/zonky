import EventBus from '../../event-bus'
import axios from 'axios'
import ZonkieSupport from './zonkieSupport.html'

export default ZonkieSupport({
  name: 'ZonkieSupport',

  data: () => ({
    support: [{
      name: '',
      date: '',
      title: '',
      mail: '',
      body: ''
    }]
  }),

  methods: {
    validateBeforeSubmit () {
      this.$validator.validateAll().then((result) => {
        if (!result) {
          console.log('You have one, or more validation errors ', result)
          return
        }

        axios.post('https://jsonplaceholder.typicode.com/posts', {
          userId: this.support.name,
          id: this.support.email,
          title: this.support.title,
          body: this.support.body
        })
          .then(function (response) {
            console.log('Message successfully sent')
            console.log(response)
            EventBus.$emit('trigger-modal', false)
          })
          .catch(function (error) {
            console.log(error)
          })
      })
    },

    disabledDates: date => {
      const day = new Date()

      return day < date
    }
  }
})
