import AppRoot from './appRoot.html'
import LoanRatingFilter from '../loanRatingFilter/loanRatingFilter'
import LoanAverageCalculator from '../loanAverageCalculator/loanAverageCalculator'
import LoanList from '../loanList/loanList'

export default AppRoot({
  name: 'root',

  components: {
    LoanRatingFilter,
    LoanAverageCalculator,
    LoanList
  }
})
