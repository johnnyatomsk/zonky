import EventBus from '../../event-bus'
import LoanAverageCalculator from './loanAverageCalculator.html'

export default LoanAverageCalculator({
  name: 'LoanAverageCalculator',

  data: () => ({
    loansData: {},
    filteredLoans: {},
    averageLoanAmount: 0
  }),

  computed: {
    getVuexLoansData () {
      return this.$store.state.loans.loansData
    },

    getLoanRating () {
      return this.$store.state.loans.loanRating
    },

    getLoanRatingCode () {
      return this.$store.state.loans.loanRatingCode
    }
  },

  created () {
    this.makeObject()
  },

  methods: {
    makeObject () {
      let loans = []

      setTimeout(() => {
        for (let i = 0; i < this.getVuexLoansData.length; i++) {
          loans.push({
            ratings: this.getVuexLoansData[i].rating,
            amount: this.getVuexLoansData[i].amount
          })
        }
      }, 500)

      this.loansData = loans
    },

    countAverageLoanAmount () {
      let filteredLoans = this.getVuexLoansData
      let filteredLoansAmounts = 0
      let filteredLoansCount = filteredLoans.length

      for (let i = 0; i < filteredLoans.length; i++) {
        filteredLoansAmounts += filteredLoans[i].amount
      }

      this.averageLoanAmount = filteredLoansAmounts / filteredLoansCount
    }
  },

  mounted () {
    EventBus.$on('trigger-calculator', countLoanAmount => {
      this.countAverageLoanAmount()
    })
  }
})
